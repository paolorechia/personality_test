import os
import csv
import pprint
import sys
import pickle
import argparse
import re
from directories import FilepathesDB

    # Program B

    # Read preprocessed text - done
    # (Opt) Implement reading mypersonality_j for lexycal vector - done
    # Merge lexycal vector into author_db - done

def build_lexycal_array(row, fieldnames, ignore):
    lexycal_array = []
    for field in fieldnames: 
        if field not in (ignore):
            lexycal_array.append(row[field].replace(",","."))

    return lexycal_array

# Only exists for mypersonality for now
def read_lexycal(csv_filepath, lexycal_type = 'mypersonality_j'):
    # Mypersonality has a lexycal analysis too, let's read it.
    with open(csv_filepath, "r") as csv_lexycal:
        reader = csv.DictReader(csv_lexycal, delimiter=';')
#        print reader.fieldnames
        ignore = ['A', 'B']
        lexycal_dict = {}
        if lexycal_type == 'mypersonality_j':
            # Figure a way to read values in order and not in random order! :(
            for row in reader:
                lexycal_array = build_lexycal_array(row, reader.fieldnames, ignore)
                lexycal_dict[row['A']] = lexycal_array

        elif lexycal_type == 'mypersonality_s':
            for row in reader:
                lexycal_array = build_lexycal_array(row, reader.fieldnames, ignore)
                if row['A'] in lexycal_dict:
                    lexycal_dict[row['A']].append(lexycal_array)
                else:
                    lexycal_dict[row['A']] = []
                    lexycal_dict[row['A']].append(lexycal_array)
    return lexycal_dict

if __name__ == "__main__":
    # Read lexycal_type from command line
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('lexycal_type', metavar='c', type=str)
    args = parser.parse_args()
    lexycal_type = args.lexycal_type

    filepathes = FilepathesDB()
    lexycal_dict = read_lexycal(
                                 filepathes.fetch_csv_path(lexycal_type),
                                 lexycal_type
                                )
    # lexycal_dict
#    print lexycal_dict
    print("{0} unique keys found.".format(len(list(lexycal_dict.keys()))))
    
    fp = filepathes.preprocessing_filepath(lexycal_type[:-2])
    author_db = pickle.load(open(fp, 'rb'))
    print("{0} unique keys read from db.".format(len(list(author_db.authors.keys()))))

    if (lexycal_type == 'mypersonality_s'):
        for key, value in lexycal_dict.items():
            i = 0
            for subitem in value:
                try:
                    author_db.authors[key].comments[i].set_lexycal_vector(subitem)
                    i += 1
                except:
                    sys.exit("{0} not found.".format(key))

    
    print("Sucessfully merged dictionaries.")

    print(author_db.authors['b7b7764cfa1c523e4e93ab2a79a946c4'].comments[0].preprocessed_text)
    print(author_db.authors['b7b7764cfa1c523e4e93ab2a79a946c4'].comments[0].lexycal_vector)

    author_db.pickle_save(filepathes.preprocessing_filepath(lexycal_type[:-2]))
