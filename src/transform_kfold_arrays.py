import os
import csv
import pprint
import sys
import pickle
import argparse
import re

from directories import FilepathesDB
import csv_classes

if __name__ == "__main__":
    # Read database from command line
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('database', metavar='c', type=str)
    args = parser.parse_args()

    filepathes = FilepathesDB()
    fp = filepathes.preprocessing_filepath(args.database)
    author_db = pickle.load(open(fp, 'rb'))
    print("{0} unique keys read from db.".format(len(list(author_db.authors.keys()))))

    filepathes = FilepathesDB()
    for k in [3, 5, 10]:
        path = filepathes.kfold_array(k)
        exec(compile(open(path).read(), path, 'exec'))
        new_array = indiceKFOLD
        pickle_output = filepathes.kfold(k)
        with open(pickle_output, 'wb') as output:
                pickle.dump(new_array, output, pickle.HIGHEST_PROTOCOL)
        print("Finished saving to {0}".format(pickle_output))
         
