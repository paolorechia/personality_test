    # Program C - base code from http://nadbordrozd.github.io/blog/2016/05/20/text-classification-with-word2vec/

    # Read feature vector structure (if possible, keeping k-fold splitting)

    # Then feed into machine learning/Deep learning program

    # Generate metrics (report, graphs)

#get http://nlp.stanford.edu/data/glove.6B.zip
#unzip glove.6B.zip

#and use load them up in python:

# My Code
import csv_classes
import clf_utils as ut
import latex
import charter
from directories import FilepathesDB

# Libs
import argparse
import numpy as np
import gensim
import pickle
import sys
import os
import re
from matplotlib import pyplot as plt

class Result():
    def __init__(self, filename, model_name):
        self.filename = filename
        self.trimmed  = filename.replace(model_name + "_", '')
        self.iterations = self.trimmed.split("_")[0]
        self.trait = self.trimmed.split("_")[1]
        kfold_int = ""
        for char in self.trimmed.split("_")[2]:
            if char.isdigit():
                kfold_int += char
        self.kfold= int(kfold_int)


def read_results(model_name, lexycal_flag = False):
    filepathes = FilepathesDB()
    full_list = os.listdir(filepathes.results_dir)
    lex_filtered = []
    for ele in full_list:
        if lexycal_flag and "lexycal" in ele:
            lex_filtered.append(ele)
        elif not lexycal_flag and "lexycal" not in ele:
            lex_filtered.append(ele)
                
    model_list = []
    for element in lex_filtered:
        if model_name in element:
            model_list.append(element)

            
#    print model_list
    empty_p = csv_classes.Personality('y','y','y','y','y')
    by_trait_dict = {}
    for element in model_list:
        result_file = Result(element, model_name)
#        print result_file.iterations, result_file.trait, result_file.filename
        try:
            by_trait_dict[result_file.trait].append(result_file)
        except KeyError:
            by_trait_dict[result_file.trait] = []
            by_trait_dict[result_file.trait].append(result_file)
    return by_trait_dict


def iterate_dict(by_trait_dict):
    filepathes = FilepathesDB()
    result_dict = {}
    for personality in list(by_trait_dict.keys()):
        by_trait_dict[personality].sort(key=lambda x: int(x.iterations))
        array_of_means = []
        for result in by_trait_dict[personality]:
            pkl_file = open(os.path.join(filepathes.results_dir, \
                                          result.filename), 'rb'
                            )
            pkl_result = pickle.load(pkl_file)
#            print result.iterations
            ordered_classifiers = []
            unzipped = list(zip(*pkl_result))
            mean_array = []
            for i in range(0, result.kfold):
                value_array = []
                name = unzipped[i][0][0]
                for ele in unzipped[i]:
                    value_array.append(ele[1])
                mean_array.append((result.iterations, name, np.mean(value_array)))
            array_of_means.append(mean_array)
        result_dict[personality]=array_of_means
    return result_dict

def print_result(result_dict, k = 3):
    for personality, arrays in result_dict.items():
        print(personality)
        for mean_array in arrays:
            print(mean_array)

    

def chart_result(result_dict, model_name):
    fig, axarray = plt.subplots(2,3, sharex=True)
    i = 0
    j = 0
    for personality, arrays in result_dict.items():
        print(personality)
        axes = []
        clf_1 = []
        clf_2 = []
        clf_3 = []
        iter_array = []
        for mean_array in arrays:
            name_1 = mean_array[0][1]
            name_2 = mean_array[1][1]
            name_3 = mean_array[2][1]
            clf_1.append(mean_array[0][2])
            clf_2.append(mean_array[1][2])
            clf_3.append(mean_array[2][2])
            iter_array.append(int(mean_array[0][0]))
        print(iter_array)
#        axarray[i, j].set_xticks(np.arange(min(iter_array), max(iter_array)+ 10))
        axarray[i, j].plot(clf_1, label=name_1)
        axarray[i, j].plot(clf_2, label=name_2)
        axarray[i, j].plot(clf_3, label=name_3)
        axarray[i, j].set_title(personality)
#        axarray[i, j].legend()
#        plt.xticks(np.arange(len(iter_array), iter_array))
        j += 1
        if j == 3:
            j = 0
            i += 1
    axarray[i, j].plot([0], label=name_1)
    axarray[i, j].plot([0],label=name_2)
    axarray[i, j].plot([0],label=name_3)
    axarray[1, 2].legend()
    plt.show()
    fig.savefig(model_name + ".pdf")

        

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('model_name', metavar='m', type=str)
    args = parser.parse_args()
    result_dict = read_results(args.model_name, lexycal_flag = False)
    result_dict2 = iterate_dict(result_dict)
    print_result(result_dict2)
    chart_result(result_dict2, args.model_name)
