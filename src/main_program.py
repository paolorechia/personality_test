#!/bin/python

from subprocess import call
import csv_classes

# Only needs to run processing once! Or, when change comes"

    # Program A - CSV Preprocessing

    # python csv_preprocessing.py
    # Read CSV - True - done
    # Build a dictionary/list of author_ID + essay - done
    # Check for duplicates - done
    # First preprocessing it - done
    # Store proprocessed text - done 
    # Refactor to read different input files - done
    # Extra: Refactor into OOP - done

    # Program B 
    # python lexycal_fetching.py
    # Read preprocessed text
    # (Opt) Implement reading mypersonality_j for lexycal vector
    # Stores lexycal vector on author dictionary

    # ----> Fit this somewhere after this point, but before Machine Learning.
    ######### Read chosen K-Fold and split data

def preprocess_pipeline():
    call(["python", "csv_preprocessing.py", "mypersonality"])
    call(["python", "lexycal_fetching.py", "mypersonality_s"])


# Right now only works for mypersonality.
# To work with essay:
# 1. Add essayIterator to csv_classes
# 2. Use it somehow on word2vec and on feature_vector

# Main pipeline: model-> feature vector -> machine learning
def deprecated_example_pipeline():
    iterations=200
    model_name = "Test_pipeline"
    db = "mypersonality"
    call(["python", "word2vec.py", db, model_name, str(iterations)])
    model_name += "_" + str(iterations)
    call(["python", "feature_vector.py", model_name])
    
    call(["python", "export2svm.py", model_name])
    # Export generates 2 .svm files (normal and with lexycal vector)
    call(["python", "machine_learning.py", model_name])
    model_name += "_lexycal"
    call(["python", "machine_learning.py", model_name])

def test_1():

    iterations_list = [500, 1000, 3500, 6500, 10000, 20000, 50000, 100000, 500000, 1000000]
    db = "mypersonality"
    model_base_name = "Test_1"
    for iterations in iterations_list:
        call(["python", "word2vec.py", db, model_base_name, str(iterations)])
        model_name = model_base_name + "_" + str(iterations)
        call(["python", "feature_vector.py", model_name])
    k = 3
    empty_p = csv_classes.Personality('y','y','y','y','y')
    for p_trait in list(empty_p.dict.keys()):
        for iterations in iterations_list:
            model_name = model_base_name + "_" + str(iterations)
            call(["python", "export2svm.py", model_name, p_trait, str(k)])
            # Export generates 2 .svm files (normal and with lexycal vector)
            export_name = model_name + "_" + str(p_trait) + "_kfold" + str(k)
            call(["python", "machine_learning.py", export_name])
            export_name += "_lexycal"
            call(["python", "machine_learning.py", export_name])

def test_2():

    db = "mypersonality"
    model_base_name = "Test_2"
#    for iterations in range(10, 200, 10):
#        call(["python", "word2vec.py", db, model_base_name, str(iterations)])
#        model_name = model_base_name + "_" + str(iterations)
#        call(["python", "feature_vector.py", model_name])
    k = 3
    empty_p = csv_classes.Personality('y','y','y','y','y')
    for p_trait in list(empty_p.dict.keys()):
        for iterations in range(10, 200, 10):
            model_name = model_base_name + "_" + str(iterations)
            call(["python", "export2svm.py", model_name, p_trait, str(k)])
            # Export generates 2 .svm files (normal and with lexycal vector)
            export_name = model_name + "_" + str(p_trait) + "_kfold" + str(k)
            call(["python", "machine_learning.py", export_name])
#            export_name += "_lexycal"
#            call(["python", "machine_learning.py", export_name])

# A fine idea for a program:
# Master model maker: creates models if and only if model does not exist
# Master exporter: same, only exports if destination file does not yet exist.
# Both may run on overwrite mode.
# Both should manage file creation
# Maybe a master manager for each program? This should speed up tests a little bit.
# Apply paralellism? : )

def main():
    print("Hello, main!")
    # Full Steps
    
    test_2()
    
    # An idea...
    # Use pickle to store machine_learning results?
    # Then process results in another program to generate latex, charts etc
    # This allows one to effectively walk through different models
    # And word2vec iterations


    # Program C

    # python word2vec.py
    # Train word2vec model on text (maybe add more models?)
    # Save model


    # Program D

    # python feature_vector.py
    # Load word2vec model
    # Save feature vector

    # Program E

    # python export2svm.py
    # Load Database
    # Export feature vectors to .SVM files

    # Program F

    # python machine_learning.py
    # Read feature vector structure
    # Then feed into machine learning/Deep learning program
    # Generate metrics (report, graphs)

if __name__ == "__main__":
    main()

