#!/bin/python


import os
import csv
import pprint
import sys
import nltk
import logging
import pickle
import argparse
import gensim

from csv_classes import *
from directories import FilepathesDB


    # Program A - 

    # Read CSV - True - done

    # Build a dictionary/list of author_ID + essay - done

    # Check for duplicates - done

    # Build Interface to transform text - partially done

    # First preprocessing it - done

    # Store proprocessed text - done 

    # Refactor to read different input files - done

    # Extra: Refactor into OOP - done

    # Extra: Refactor filepathes - done



logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

# Preprocessing methods
def gensim_nltk(text):
    clean_text = []
    preprocessed = gensim.utils.simple_preprocess(text)
    for word in preprocessed:
        if word not in nltk.corpus.stopwords.words('english'):
            clean_text.append(word)
    return clean_text



def read_csv(author_db, csv_filepath, csv_type = 'essay', lexycal_type = 'mypersonality_j'):
    with open(csv_filepath, "r",  encoding = "ISO-8859-1") as csv_essays:
        reader = csv.DictReader(csv_essays)
        read_lines = 0
        for row in reader:
            personality = Personality(row['cEXT'], row['cNEU'], row['cAGR'], row['cCON'], row['cOPN'])
            author = Author(row['#AUTHID'], personality)
            if csv_type == 'essay':
                #AUTHID,TEXT,cEXT,cNEU,cAGR,cCON,cOPN
                text = Text(row['TEXT'])
                author.insert_essay(text)
            elif csv_type == 'mypersonality':
                #AUTHID,STATUS,cEXT,cNEU,cAGR,cCON,cOPN
                text = Text(row['STATUS'])
                author.insert_comment(text)
            author_db.insert_author(author)
            read_lines += 1
            
def csv_preprocessing(csv_type):
    # Greetings!
    print("Hello, preprocessing!")

    # Import base directories
    filepathes = FilepathesDB()

    # Set input/output filepath
    csv_filepath = filepathes.fetch_csv_path(csv_type)
    pickle_output = filepathes.preprocessing_filepath(csv_type)

    # Initialize Author Dictionary
    author_db = AuthorDict()

    # Read input as structured Data
    print("Attempting to read... {0}".format(csv_filepath))
    read_csv(author_db, csv_filepath, csv_type)

#    print "{0} authors found.".format(author_db.num_authors())
    print("{0} authors found.".format(len(author_db)))

    # Preprocess input text
    author_db.preprocess_all(gensim_nltk)

    commentIterator = CommentsIterator(author_db)
    i = 0
    for comment in commentIterator:
        i += 1
    print("{0} comments preprocessed.".format(i))

    # Save obj
    author_db.pickle_save(pickle_output)


if __name__ == "__main__":
    # Read csv_type from command line
    parser = argparse.ArgumentParser(description='Read a CSV file, preprocess it and stores result in pickle dump')
    parser.add_argument('csv_type', metavar='c', type=str)

    args = parser.parse_args()
    csv_type = args.csv_type
    csv_preprocessing(csv_type)
