from matplotlib import pyplot as plt
import numpy as np

from directories import FilepathesDB


def plot_bar(test_array, array_names, out_name):
    f = plt.figure()

    ax = f.add_subplot(111)

    ax.set_xlabel('Classifier')
    ax.set_ylabel('Accuracy')
    ax.set_yticks(np.arange(0, len(array_names)), array_names)
    ax.set_yticklabels(array_names)

    ax.barh(np.arange(0, len(array_names)), test_array, label="Test Accuracy")

    plt.legend()

    for i, v in enumerate(test_array):
       ax.text(i, v+0.05, str(v), color='blue', fontweight='bold')

    for i in range(0, 10):
        for j in range(0, 10):
            ax.text(i, j, "Test[{0},{1}".format(str(i), str(j)))
    plt.show()
    plt.close()

    
    filepathes = FilepathesDB()
    chart_fp = filepathes.chart("test_chart")
    f.savefig(chart_fp, bbox_inches='tight')

    print("Best Clf: {0} ({1})".format(
                                        array_names[np.argmax(test_array)], 
                                        max(test_array)
                                      ))
    print("Wrst Clf: {0} ({1})".format(
                                        array_names[np.argmin(test_array)],
                                        min(test_array)
                                       ))
if __name__ == "__main__":
    plot_bar(svm_name)
