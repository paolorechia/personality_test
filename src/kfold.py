import os
import csv
import pprint
import sys
import pickle
import argparse
import re

from directories import FilepathesDB
import csv_classes

    # Program N
    # Kfold
class KFoldClass():
    def __init__(self, k, kfold_array, author_db):
        self.author_db = author_db
        self.k = k
        self.index_array = kfold_array
        self.partitions = []
        self.train_partitions = []
        self.test_partitions = []

    def split(self):
        for i in range(0, self.k):
            self.partitions.append(csv_classes.AuthorDict())
            for index in self.index_array[i]:
                self.partitions[i].insert_author(self.author_db.get_author(index))

    def merge(self, i, j):
        merged = csv_classes.AuthorDict()
        merged.authors = self.partitions[i].authors.copy()
        merged.authors.update(self.partitions[j].authors)
        return merged

    def generate_merges(self):
        w = 0
        for i in range(self.k):
            for j in range(i + 1, self.k):
                self.train_partitions.append(self.merge(i, j)) 
                # Always yields only one test partition
                for k in range(self.k):
                    if k != i and k != j:
                        self.test_partitions.append(self.partitions[k])

def test_kfold(database):
    filepathes = FilepathesDB()
    fp = filepathes.preprocessing_filepath(database)
    author_db = pickle.load(open(fp, 'rb'))
    print("{0} unique keys read from db.".format(len(list(author_db.authors.keys()))))
    # Testing kfold
    for k in [3]:
        kfold_array = pickle.load(open(filepathes.kfold(k), 'rb'))
        kfold = KFoldClass(k, kfold_array, author_db)
        kfold.split()
        kfold.generate_merges()

    for u in range(0, k): 
        r = kfold.train_partitions[u]
        t = (kfold.test_partitions[u])
        print(len(r) + len(t), len(r), len(t))

if __name__ == "__main__":
    # Read database from command line
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('op', metavar='o', type=str)
    parser.add_argument('database', metavar='c', type=str, nargs='?')
    args = parser.parse_args()
    if args.op == 'pickle':
        build_pickle()
    elif args.op == 'test':
        test_kfold(args.database)
