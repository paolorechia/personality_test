import numpy as np
import random
import sys
import clf_utils as ut
from sklearn.metrics import classification_report


def cross(a, b, crossing):
#    print "CROSS START"
#    print "a: {}".format(a[0:6])
#    print "b: {}".format(b[0:6])
#    print "c: {}".format(crossing[0:6])
    new_a = np.copy(a)
    new_b = np.copy(b)
    for index, each in enumerate(crossing):
        if each:
            aux = new_a[index]
            new_a[index]=new_b[index]
            new_b[index]=aux
#    print "new_a: {}".format(new_a[0:6])
#    print "new_b: {}".format(new_b[0:6])
#    print "CROSS END"
    return new_a, new_b

def mutate(a):
    mutated_gene = random.randint(0, len(a) - 1)
    if a[mutated_gene]:
        a[mutated_gene]=False
    else:
        a[mutated_gene]=True
        

def print_bool_array(array):
    for each in array:
        if each:
            sys.stdout.write("1 ")
        else:
            sys.stdout.write("0 ")
    sys.stdout.write("\n")


def label_pair_score(score, labels):
    return np.mean([score[4][labels[0]][1], score[4][labels[1]][1]])

def label_score(score, label):
    return score[4][label][1]

def ica_mean(score, stub_var):
    ica = score[4]
    label, percentile= list(zip(*ica))
    return np.mean(percentile)

def ica_mean_simpler(icam_array):
    label, percentile= list(zip(*icam_array))
    return np.mean(percentile)
    

def test_score(score, stub_var):
    return score[1] 

def evolve_clf(clf, X_train, y_train, X_test, y_test, X_val, y_val, generations=10,
score_func = label_score, label=None):
#    print X_train.shape
    array_size = X_train.shape[1]
    # Mask is a chromossome
    pop_size = 6 #len(y_train) - 1
    population = []
    strongest = 0
    strongest_score = 0
    strongest_chromossome=0

    for i in range(0, pop_size):
        chromossome = np.random.choice([True, False], array_size)
        population.append(chromossome)
        score,fitted_clf = ut.testClassifier(clf, X_train[:,chromossome], y_train, X_test[:,chromossome], y_test, X_val[:,chromossome], y_val, bagging=False)
        if label is None:
            score = score_func(score, None)
        else:
            score = score_func(score, label)
        if score > strongest_score:
            strongest_score = score
            strongest = i
            strongest_chromossome = chromossome
#        print_bool_array(chromossome)
        print(score, i)
    print("Best: %d - %.2f"%(strongest, strongest_score))

    print("Best of all: %d - %.2f"%(strongest, strongest_score))
#    return strongest_chromossome

    print("Crossing start")
    for generation in range(0, generations):
        crossing = np.random.choice([True], array_size)
        crossing[random.randint(0, len(crossing) - 1)] = False
        for j in range(0, random.randint(2, pop_size/2)):
            k, u = random.randint(0, pop_size -1), random.randint(0, pop_size - 1)
            while k == u or k == strongest or u == strongest: 
                k, u = random.randint(0, pop_size - 1),  random.randint(0, pop_size - 1)
            population[k], population[u]= cross(population[k], population[u], crossing)
        for i in range(0, pop_size):
#            print_bool_array(population[i])
            score_array,fitted_clf = ut.testClassifier(clf, X_train[:,population[i]], y_train, X_test[:,population[i]], y_test, X_val[:,population[i]], y_val, bagging=False)
            if label is None:
                score = score_func(score_array, None)
            else:
                score = score_func(score_array, label)
            print(score, i)
            if float(score) > float(strongest_score):
                strongest_score = score
                strongest = i
                strongest_chromossome = chromossome
        print("Best gen(%d): %d - %.2f"%(generation, strongest, strongest_score))

    print("Best of all: %d - %.2f"%(strongest, strongest_score))
    return strongest_chromossome
            
        
def mutate_clf(clf, X_train, y_train, X_test, y_test, X_val, y_val, generations=10,
score_func = label_score, label=None):
#    print X_train.shape
    array_size = X_train.shape[1]
    # Mask is a chromossome
    pop_size = 6 #len(y_train) - 1
    population = []
    strongest = 0
    strongest_score = 0
    strongest_chromossome=0
    best_fit = None
    for i in range(0, pop_size):
        chromossome = np.random.choice([True, False], array_size)
        population.append(chromossome)
        score,fitted_clf = ut.testClassifier(clf, X_train[:,chromossome], y_train, X_test[:,chromossome], y_test, X_val[:,chromossome], y_val, bagging=False)
        if label is None:
            score = score_func(score, None)
        else:
            score = score_func(score, label)
        if score > strongest_score:
            strongest_score = score
            strongest = i
#            strongest_chromossome = chromossome
            strongest_chromossome = np.array(population[i], copy=True)
            best_fit = fitted_clf
#        print_bool_array(chromossome)
#        print score, i
    print("Best: %d - %.2f"%(strongest, strongest_score))

    print("Best of all: %d - %.2f"%(strongest, strongest_score))

    print("Mutation start")
    for generation in range(0, generations):
        crossing = np.random.choice([True], array_size)
        crossing[random.randint(0, len(crossing) - 1)] = False
        for j in range(0, random.randint(2, pop_size/2)):
            k, u = random.randint(0, pop_size -1), random.randint(0, pop_size - 1)
            mutate(population[k])
            mutate(population[u])
#            print "mutated %d"%(k)
        for i in range(0, pop_size):
#            print_bool_array(population[i])
            score_array,fitted_clf = ut.testClassifier(clf, X_train[:,population[i]], y_train, X_test[:,population[i]], y_test, X_val[:,population[i]], y_val, bagging=False)
            if label is None:
                score = score_func(score_array, None)
            else:
                score = score_func(score_array, label)
#            print score, i
            if score > strongest_score:
                strongest_score = score
                strongest = i
                strongest_chromossome = np.array(population[i], copy=True)
                best_fit = fitted_clf
        print("Best gen(%d): %d - %.6f"%(generation, strongest, strongest_score))

    print("Best of all: %d - %.6f"%(strongest, strongest_score))
    score_array, fitted_clf = ut.testClassifier(clf, X_train[:,strongest_chromossome], y_train, X_test[:,strongest_chromossome], y_test, X_val[:,strongest_chromossome], y_val, bagging=False)
    ut.print_score(score_array)
    ut.score_to_latex(score_array)
    return strongest_chromossome, fitted_clf

# bf = brute force
def genetic_window_bf_evolution(clf, X_train, y_train, X_test, y_test, X_val, y_val, step=256, score_func=ica_mean, label=None, print_res=True):

    score, fitted_clf = ut.testClassifier(clf, X_train, y_train, X_test, y_test, X_val, y_val, bagging=False)
    if print_res:
        print("Original feature set")
        ut.print_score(score)
        ut.score_to_latex(score)
    
    max_score = 0
    best_i = 0
    best_j = 0 
    limit = X_train.shape[1]
    for i in range(0, limit, step):
        for j in range(i + step, limit, step):
#            print "Using %d features." %(j - i)
            X_cutted_train = X_train[:, i:j]
            X_cutted_test = X_test[:, i:j]
            X_cutted_val = X_val[:, i:j]
            score_array,fitted_clf = ut.testClassifier(clf, X_cutted_train, y_train, X_cutted_test, y_test, X_cutted_val, y_val, bagging=False)
            if label is None:
                score = score_func(score_array, None)
            else:
                score = score_func(score_array, label)
            if score > max_score:
                max_score = score
                best_i= i
                best_j = j

    X_cutted_train = X_train[:, best_i:best_j]
    X_cutted_test = X_test[:, best_i:best_j]
    X_cutted_val = X_val[:, best_i:best_j]
    score,fitted_clf = ut.testClassifier(clf, X_cutted_train, y_train, X_cutted_test, y_test, X_cutted_val, y_val, bagging=False)
    if print_res:
        print("Best:")
        print("Max_score: %.2f"%(max_score))
        print("Indexes (i: %d)(j: %d)"%(best_i, best_j))
        ut.print_score(score)
        ut.score_to_latex(score)
    return best_i, best_j
        

def genetic_bf_crossing(clf, X_train, y_train, X_test, y_test, X_val, y_val, window_start, window_end):
    X_cutted_train = X_train[:, window_start:window_end]
    X_cutted_test = X_test[:, window_start:window_end]
    X_cutted_val = X_val[:, window_start:window_end]
    evolution.evolve_clf(clf, X_train, y_train, X_test, y_test, X_val, y_val, generations=10)
    score,fitted_clf = ut.testClassifier(clf, X_cutted_train, y_train, X_cutted_test, y_test, X_cutted_val, y_val, bagging=False)
    ut.print_score(score)


def window_plus_mutation(algorithm, X_train, y_train, X_test, y_test, X_val, y_val, window_step = 128, generations=20, score_func=test_score, label=None):
    best_i, best_j = genetic_window_bf_evolution(algorithm, X_train, y_train, X_test, y_test, X_val, y_val, score_func = score_func, label=label, step=window_step)
    best_chromossome, fitted_clf = mutate_clf(algorithm, X_train[:,best_i:best_j], y_train, X_test[:,best_i:best_j], y_test, X_val[:,best_i:best_j], y_val, score_func = score_func, label=label, generations=generations)
#    score_array, fitted_clf = ut.testClassifier(fitted_clf, X_train[:,best_chromossome], y_train, X_test[:,best_chromossome], y_test, X_val[:,best_chromossome], y_val, bagging=False)
#    ut.print_score(score_array)

def window_plus_evolution(algorithm, X_train, y_train, X_test, y_test, X_val, y_val, window_step = 128, generations=20, score_func=test_score, label=None):
    best_i, best_j = genetic_window_bf_evolution(algorithm, X_train, y_train, X_test, y_test, X_val, y_val, score_func = score_func, label=label, step=window_step)
    best_chromossome= evolve_clf(algorithm, X_train[:,best_i:best_j], y_train, X_test[:,best_i:best_j], y_test, X_val[:,best_i:best_j], y_val, score_func = score_func, label=label, generations=generations)

# Experimental procedure
def try_diff_combos(clf, X_train, y_train, X_test, y_test, X_val, y_val):
    window_step = 1024 
    generations = 1
    results = []
    combos = [(0,1),(0,2),(0,3),(0,4),
              (1,2),(1,3),(1,4),
              (2,3),(2,4),
              (3,4)]
    
    for labels in combos:
        best_i, best_j = evolution.genetic_window_bf_evolution(algorithm, X_train, y_train, X_test, y_test, X_val, y_val, score_func = evolution.label_pair_score, label=labels, step=window_step)
        best_chromossome, fitted_clf = evolution.mutate_clf(algorithm, X_train[:,best_i:best_j], y_train, X_test[:,best_i:best_j], y_test, X_val[:,best_i:best_j], y_val, score_func = evolution.label_pair_score, label=labels, generations=generations)
#        print best_chromossome
        score_array, fitted_clf = ut.testClassifier(fitted_clf, X_train[:,best_chromossome], y_train, X_test[:,best_chromossome], y_test, X_val[:,best_chromossome], y_val, bagging=False)
        results.append((labels, best_chromossome, fitted_clf, score_array))

    for labels, chromossome, clf , score_array in results:
        print(labels)
        ut.print_score(score_array)
        ut.score_to_latex(score_array)

    sys.exit()
    cutted_X = X_test[:,results[0][1]]
    test = int(results[0][2].predict(cutted_X)[0])

    wc = wombo_classifier.Wombo_classifier(results)
    wc.predict(X_test, y_test)

#if __name__ == "__main__":
#    array_size = 16
#    samples = 4
#    feature_vector = np.random.randint(2, size=(samples,array_size))
#    labels = np.random.randint(4, size=array_size)
#    evolve_clf(feature_vector, labels, None)



def bf_window_icam(clf, X_train, y_train, X_test, y_test, X_val, y_val, step=256, print_res=True):

    score, fitted_clf = ut.test_clf_icam(clf, X_train, y_train, X_test, y_test, X_val, y_val, bagging=False)
#    if print_res:
#        print "Original feature set"
##        ut.print_icam_score(score)
    
    max_score = 0
    best_i = 0
    best_j = 0 
    limit = X_train.shape[1]
    icam_matrix = []
    for i in range(0, limit - 1, step):
        for j in range(i + step, limit, step):
#            print i, j
#            print "Using %d features." %(j - i)
            X_cutted_train = X_train[:, i:j]
            X_cutted_test = X_test[:, i:j]
            X_cutted_val = X_val[:, i:j]
#            print (X_cutted_train, X_cutted_test, X_cutted_val)
            score, fitted_clf = ut.test_clf_icam(clf, X_cutted_train, y_train, X_cutted_test, y_test, X_cutted_val, y_val, bagging=False)
#            ut.print_icam_score(score)
            icam_matrix.append((i, j, ica_mean_simpler(score[0]),
                               ica_mean_simpler(score[1]),
                               ica_mean_simpler(score[2])))
            

    X_cutted_train = X_train[:, best_i:best_j]
    X_cutted_test = X_test[:, best_i:best_j]
    X_cutted_val = X_val[:, best_i:best_j]
    score,fitted_clf = ut.test_clf_icam(clf, X_cutted_train, y_train, X_cutted_test, y_test, X_cutted_val, y_val, bagging=False)
    return icam_matrix 

def mutate_icam_clf(clf, X_train, y_train, X_test, y_test, X_val, y_val, generations=10, mean=False):

#    print X_train.shape
    array_size = X_train.shape[1]
    # Mask is a chromossome
    pop_size = 6 #len(y_train) - 1
    population = []
    strongest = 0
    strongest_score = 0
    strongest_chromossome=0
    best_fit = None
    score_history = []
    for i in range(0, pop_size):
        chromossome = np.random.choice([True, False], p=[0.95, 0.05], size=array_size)
        population.append(chromossome)
        score,fitted_clf = ut.test_clf_icam(clf, X_train[:,chromossome], y_train, X_test[:,chromossome], y_test, X_val[:,chromossome], y_val, bagging=False)
        # current_score is (Test + Valid) / 2
        if(mean):
            current_score = (ica_mean_simpler(score[1]) + ica_mean_simpler(score[2]))/2
        else:
        # or just test mean
            current_score = ica_mean_simpler(score[1])
        if current_score > strongest_score:
            strongest_score = current_score
            strongest = i
            strongest_chromossome = np.array(population[i], copy=True)
            best_fit = fitted_clf
    print("Best: %d - %.2f"%(strongest, strongest_score))

    print("Best of all: %d - %.2f"%(strongest, strongest_score))
    score_history.append(strongest_score)

    print("Mutation start")
    for generation in range(0, generations):
        best_of_gen = 0
        best_of_gen_score = 0
        crossing = np.random.choice([True], array_size)
        for j in range(0, random.randint(2, pop_size/2)):
            k, u = random.randint(0, pop_size -1), random.randint(0, pop_size - 1)
            mutate(population[k])
            mutate(population[u])
#            print "mutated %d"%(k)
        for i in range(0, pop_size):
#            print_bool_array(population[i])
            score,fitted_clf = ut.test_clf_icam(fitted_clf, X_train[:,chromossome], y_train, X_test[:,chromossome], y_test, X_val[:,chromossome], y_val, bagging=False)
            # current_score is (Test + Valid) / 2
            if(mean):
                current_score = (ica_mean_simpler(score[1]) + ica_mean_simpler(score[2]))/2
            else:
            # or just test mean
                current_score = ica_mean_simpler(score[1])
            if current_score > best_of_gen_score:
                best_of_gen = i
                best_of_gen_score = current_score
            if current_score > strongest_score:
                strongest_score = current_score
                strongest = i
                strongest_chromossome = np.array(population[i], copy=True)
                best_fit = fitted_clf
                ut.print_icam_score(score)
        score_history.append(best_of_gen_score)
        print("Best of gen(%d): %d - %.6f"%(generation, best_of_gen, best_of_gen_score))
    print("Best of all: %d - %.6f"%(strongest, strongest_score))
    score_array, fitted_clf = ut.test_clf_icam(best_fit, X_train[:,strongest_chromossome], y_train, X_test[:,strongest_chromossome], y_test, X_val[:,strongest_chromossome], y_val, bagging=False)
    ut.print_icam_score(score)
    return score_history
    #return strongest_chromossome, fitted_clf

def evolve_icam_clf(clf, X_train, y_train, X_test, y_test, X_val, y_val, generations=10, pop_size=100, cross_over_p = 0.8, mutation= True, cross_over_rate = 0.9, mean=False):
#    print X_train.shape
    array_size = X_train.shape[1]
    # Mask is a chromossome
    population = []
    strongest = 0
    strongest_score = 0
    strongest_chromossome=0

    score_history = []
    for i in range(0, pop_size):
        chromossome = np.random.choice([True, False], p=[0.98, 0.02], size=array_size)
        population.append(chromossome)
        score,fitted_clf = ut.test_clf_icam(clf, X_train[:,chromossome], y_train, X_test[:,chromossome], y_test, X_val[:,chromossome], y_val, bagging=False)
        # current_score is (Test + Valid) / 2
        if(mean):
            current_score = (ica_mean_simpler(score[1]) + ica_mean_simpler(score[2]))/2
        else:
            current_score = ica_mean_simpler(score[1])
        if current_score > strongest_score:
            strongest_score = current_score
            strongest = i
            strongest_chromossome = np.array(population[i], copy=True)
            best_fit = fitted_clf
    print("Best: %d - %.2f"%(strongest, strongest_score))

    print("Best of all: %d - %.2f"%(strongest, strongest_score))
    score_history.append(strongest_score)

    print("Crossing start")
    for generation in range(0, generations):
        best_of_gen = 0
        best_of_gen_score = 0
        crossing = np.random.choice([True, False], size=array_size, p=[cross_over_p, 1 - cross_over_p])
        for j in range(0, random.randint(2, round(pop_size*cross_over_rate))):
            k, u = random.randint(0, pop_size -1), random.randint(0, pop_size - 1)
            while k == u or k == strongest or u == strongest: 
                k, u = random.randint(0, pop_size - 1),  random.randint(0, pop_size - 1)
            population[k], population[u]= cross(population[k], population[u], crossing)
            # Implement mutation here
            if (mutation):
                for z in range(0, 2): #random.randint(1, pop_size/4)):
                    m = random.randint(0, pop_size - 1)
                    while m == strongest:
                        m = random.randint(0, pop_size - 1)
                    mutate(population[m])

            k = random.randint(0, pop_size - 1)
            # Use strongest on someone
#            while k == strongest:
#                k = random.randint(0, pop_size - 1)
#            population[k] = cross(population[k], strongest_chromossome, crossing)
            

        for i in range(0, pop_size):
#            print_bool_array(population[i])
            chromossome = population[i]
            score,fitted_clf = ut.test_clf_icam(clf, X_train[:,chromossome], y_train, X_test[:,chromossome], y_test, X_val[:,chromossome], y_val, bagging=False)
            if(mean):
                current_score = (ica_mean_simpler(score[1]) + ica_mean_simpler(score[2]))/2
            else:
                current_score = ica_mean_simpler(score[1])
#            print current_score
            if current_score > best_of_gen_score:
                best_of_gen = i
                best_of_gen_score = current_score
            if current_score > strongest_score:
                strongest_score = current_score
                strongest = i
                strongest_chromossome = np.array(population[i], copy=True)
                best_fit = fitted_clf
#                ut.print_icam_score(score)
        score_history.append(best_of_gen_score)
        print("Best of gen(%d): %d - %.6f"%(generation, best_of_gen, best_of_gen_score))
    print("Best of all: %d - %.6f"%(strongest, strongest_score))
    simple_score, fitted_clf = ut.testClassifier(best_fit, X_train[:,strongest_chromossome], y_train, X_test[:,strongest_chromossome], y_test, X_val[:,strongest_chromossome], y_val, bagging=False)
    print("Simple score")
    ut.print_score(simple_score)
    score_array, fitted_clf = ut.test_clf_icam(best_fit, X_train[:,strongest_chromossome], y_train, X_test[:,strongest_chromossome], y_test, X_val[:,strongest_chromossome], y_val, bagging=False)
    print("ICAM score")
    ut.print_icam_score(score_array)
    target_names = ["bart", "homer", "lisa", "maggie", "marge"]
    y_pred = fitted_clf.predict(X_test[:,strongest_chromossome])
    y_pred2 = fitted_clf.predict(X_val[:,strongest_chromossome])
    y_global_pred = np.concatenate((y_pred, y_pred2), axis=0)
    y_global = np.concatenate((y_test, y_val))
    print((classification_report(y_global, y_global_pred, target_names=target_names)))
#    ut.my_roc_curve(fitted_clf, X_train, y_train, X_test, y_test, "Evolved Algorithm", "evolved_roc.pdf")
    return score_history
