    # Program C - base code from http://nadbordrozd.github.io/blog/2016/05/20/text-classification-with-word2vec/

    # Read feature vector structure (if possible, keeping k-fold splitting)

    # Then feed into machine learning/Deep learning program

    # Generate metrics (report, graphs)

#get http://nlp.stanford.edu/data/glove.6B.zip
#unzip glove.6B.zip

#and use load them up in python:

# My Code
import csv_classes
import clf_utils as ut
import latex
import charter
from directories import FilepathesDB

# Libs
import argparse
import numpy as np
import gensim
import pickle
import sys
import os

# Sklearn
from sklearn.datasets import load_svmlight_file
from sklearn.model_selection import train_test_split

# Machine Learning Algorithms

from sklearn.naive_bayes import BernoulliNB
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import Perceptron
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier

clf_names = [
                         "BernoulliNB",
    #                    "Decision Tree", 
    #                     "KNN", 
                         "RandomForest", 
    #                     "AdaBoost", 
    #                     "GradientBoosting",
                         "LogisticRegression",
                         "Perceptron",
    #                     "SVC",
                    ]
clf_calls = [
                         BernoulliNB(),
    #                     DecisionTreeClassifier(random_state=0), 
    #                     KNeighborsClassifier(n_neighbors=2, weights="distance"),
                         RandomForestClassifier(n_estimators=100, max_depth=2, n_jobs=8),
    #                     AdaBoostClassifier(n_estimators=100),
    #                     GradientBoostingClassifier(n_estimators=100, learning_rate=1.0, max_depth=1, random_state=0),
    #                     LogisticRegression(random_state=1),
                         Perceptron(),
    #                     SVC(),
                    ] 

def read_dir(dir_fp):
    files = os.listdir(dir_fp)
    train_files = [k for k in files if 'train' in k]
    test_files = [k for k in files if 'test' in k]
    train_files = sorted(train_files)
    test_files = sorted(test_files)
    print(train_files)
    print(test_files)
    return train_files, test_files

def ml_classifier(svm_base_name):
    filepathes = FilepathesDB()
    train_files, test_files = read_dir(filepathes.svm_dir(svm_base_name))
    X_train_array = []
    X_test_array = []
    y_train_array = []
    y_test_array = []
    # Read train stuff here
    for train in train_files:
        X, y = load_svmlight_file(filepathes.svm_filepath(svm_base_name, train))
        X_train_array.append(X) 
        y_train_array.append(y) 

    for test in test_files:
        X, y = load_svmlight_file(filepathes.svm_filepath(svm_base_name, test))
        X_test_array.append(X) 
        y_test_array.append(y) 


    result_array = []
    for i in range(0, len(train_files)):
        X_train = X_train_array[i]
        y_train = y_train_array[i]
        X_test = X_test_array[i]
        y_test = y_test_array[i]

        score_array = []
        print("Raw classifiers...")
        latex_fp = filepathes.latex(svm_base_name + "_{0}".format(i))
        with open(latex_fp, "w") as output_latex:
            for index, clf in enumerate(clf_calls):
                score, fitted_clf  = ut.testClassifier(
                                clf, 
                                X_train, 
                                y_train,
                                X_test,
                                y_test, 
                              )
                # Test score
                score_array.append(score[1])
                ut.print_clf_score(score, clf_names[index])
        #        latex.write(score, clf_names[index], output_latex)
        #        ut.multirow_latex_tabular(score, clf_names[index])
        result_array.append(list(zip(clf_names, score_array)))

#    chart_fp = filepathes.chart("test_chart")
#    charter.plot_bar(test_array, clf_names, chart_fp)

    print(result_array)
    result_fp = filepathes.result(svm_base_name)
    with open(result_fp, 'wb') as output:
            pickle.dump(result_array, output, pickle.HIGHEST_PROTOCOL)
    print("Finished saving to {0}".format(result_fp))

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('svm_file', metavar='m', type=str)
    args = parser.parse_args()
#    svm_base_name = "Test_w2v_mean"
#    svm_base_name = "glove6B.300d"
    ml_classifier(args.svm_file)
