import os
import csv
import pprint
import sys
import pickle
import argparse
import gensim
import logging

import csv_classes
import pipeline_utils as pu
from directories import FilepathesDB


number_threads=8
vector_size=300


    # Program C

    # Read preprocessed text - done

    # Apply chosen K-Fold and split data

    # Applies different feature extraction techniques, storing them.

    # Reimplement iterator for training word2vec

# Database must be mypersonality or essay
# Only implemented for mypersonality for now
def run(database, model_type, model_name, iterations):
    # Read Text
    filepathes = FilepathesDB()
    fp = filepathes.preprocessing_filepath(database)
    author_db = pickle.load(open(fp, 'rb'))
    print("{0} unique keys read from db.".format(len(list(author_db.authors.keys()))))
    # Example of iterating over all the essay objects and feeding into Word2Vec

    # Create iterator, train model 
    comments_iterator = csv_classes.CommentsIterator(author_db)
    model = gensim.models.Word2Vec(comments_iterator, iter=iterations, size=vector_size, workers=number_threads)

    # Test with one word
    print(model.wv.most_similar(positive=['semester'], topn=6))

    # Save model with name
    model_name += "_" + str(iterations)
    model_fp = filepathes.w2v_model(model_name)
    model.save(model_fp)



logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='')
#    parser.add_argument('model', metavar='m', type=str)
    parser.add_argument('database', metavar='m', type=str)
    parser.add_argument('model_name', metavar='m', type=str)
    parser.add_argument('iter', type=int, nargs='?',
                         const=True, default=False,
                         metavar='t')
    args = parser.parse_args()
    run(args.database, None, args.model_name, args.iter)
#    run(model_type = args.model, iterations=args.iter)
