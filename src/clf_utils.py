import sys
import numpy as np
import scipy
import latex
from itertools import cycle
from matplotlib import pyplot as plt

from sklearn.metrics import confusion_matrix 

def testClassifier(clf, X_train, y_train, X_test, y_test):
    clf.fit(X_train, y_train)
    train_score = clf.score(X_train, y_train)
    test_score = clf.score(X_test, y_test)
#    cria a matriz de confusao
    y_pred = clf.predict(X_test) 
    score_array = [ train_score, test_score]
    return score_array, clf

def print_score(score_array):
    print("Train score: {0}.".format(score_array[0]))
    print("Test score: {0}.".format(score_array[1]))

def print_clf_score(score_array, clf_name):
    print("{0}: {1}".format(clf_name, score_array[1]))

