import pickle

def character_to_bool(char):
    if char == 'y':
        return 1
    elif char == 'n':
        return 0
    else:
        sys.exit("Invalid character.")

class CommentsIterator():
    def __init__(self, author_db):
        self.author_db = author_db

    def __iter__(self):
        for key, author in self.author_db.authors.items():
            for comment in author.comments:
                yield comment.preprocessed_text

class RootCommentsIterator():
    def __init__(self, author_db):
        self.author_db = author_db

    def __iter__(self):
        for key, author in self.author_db.authors.items():
            for comment in author.comments:
                yield comment

# Objects that model CSV info
class Text():
    def __init__(self,text):
        self.text = text
        self.preprocessed_text = None
        self.lexycal_vector = None
        self.feature_vector = None

    def preprocess_text(self, f):
        self.preprocessed_text = f(self.text)

    def set_lexycal_vector(self, array):
        self.lexycal_vector = array

    def get_lexycal_vector(self):
        return self.lexycal_vector

    def set_feature_vector(self, array):
        self.feature_vector = array

    def vec2SVM(self, lexycal = False):
        i = 0
        master_string = ""
        for n in self.feature_vector:
            master_string += str(i) + ":" + str(n) + " "
            i += 1
        if lexycal:
            for n in self.lexycal_vector:
                master_string += str(i) + ":" + str(n) + " "
                i += 1
        return master_string
        

    def __str__(self):
        return str(self.__dict__)

class Personality():
    def __init__(self, cext, cneu, cagr, ccon, copn):
        self.dict = {
            "cext" : character_to_bool(cext),
            "cneu" : character_to_bool(cneu),
            "cagr" : character_to_bool(cagr),
            "ccon" : character_to_bool(ccon),
            "copn" : character_to_bool(copn)
        }
    
class Author():
    # Author id and personality
    def __init__(self, author_id, personality):
        self.id = author_id
        self.personality = personality
        self.comments = []
        self.essay = None

    # A single author may have several comments 
    def insert_comment(self, comment):
        self.comments.append(comment)

    # But only one essay
    def insert_essay(self, essay):
        self.essay = essay

    def preprocess_text(self, f):
        if self.essay is not None:
            self.essay.preprocess_text(f)
        for comment in self.comments:
            comment.preprocess_text(f)

    def export_comments2svm(self, trait, file_p, lexycal):
        if trait not in list(self.personality.dict.keys()):
            sys.exit("{0} is not a valid personality trait. Pick one \
                    from {1}.".format(trait_list))
        for comment in self.comments:
            master_string = "{0} {1}\n".format(self.personality.dict[trait], \
                                             comment.vec2SVM(lexycal))
            file_p.write(master_string)
            
    def export_essay_svm(self):
        print("Dummy method")


        


class AuthorDict():
    def __init__(self):
        self.authors = {}

    def __iter__(self):
        for key, author in self.authors.items():
            yield author

    def __len__(self):
        return len(list(self.authors.keys()))

    def insert_author(self, author):
        # Author already exists! Consider merging comments
        if author.id in list(self.authors.keys()):
            # Merge comments
            self.authors[author.id].comments += author.comments
        else:
            # Create new entry
            self.authors[author.id] = author

    def get_author(self, author_id):
        return self.authors[author_id]

    def preprocess_all(self, f):
        for key, author in self.authors.items():
            author.preprocess_text(f)
        print("Finished preprocessing.")

    def pickle_save(self, pickle_output):
        print("Saving data...")
        with open(pickle_output, 'wb') as output:
                pickle.dump(self, output, pickle.HIGHEST_PROTOCOL)
        print("Finished saving to {0}".format(pickle_output))

    def merge(self, new_db):
        self.authors.update(new_db.authors)
