    # Program C - base code from http://nadbordrozd.github.io/blog/2016/05/20/text-classification-with-word2vec/

    # Read feature vector structure (if possible, keeping k-fold splitting)
    # Then feed into machine learning/Deep learning program
    # Generate metrics (report, graphs)

#get http://nlp.stanford.edu/data/glove.6B.zip
#unzip glove.6B.zip

#and use load them up in python:
import argparse
import numpy as np
import gensim
import pickle
import sys
import os

import csv_classes
from kfold import KFoldClass
from directories import FilepathesDB
import pipeline_utils as pu

def run(feature_name, trait, k):
    filepathes = FilepathesDB()
#    fp = filepathes.preprocessing_filepath("mypersonality")
    
    fp = filepathes.feature_filepath(feature_name)
    print("Opening {0}...".format(fp))
    author_db = pickle.load(open(fp, 'rb'))

    kfold_array = pickle.load(open(filepathes.kfold(k), 'rb'))
    kfold = KFoldClass(k, kfold_array, author_db)

    kfold.split()
    kfold.generate_merges()
    
    feature_name += "_" + trait + "_kfold" + str(k)

    filepathes.build_svm_dir(feature_name)
    # Ignore lexycal
    for u in range(k):
        # Export train datasets
        svm_name = feature_name + "_train_" + str(u)
        svm_fp = filepathes.svm_filepath(feature_name, svm_name)
        with open(svm_fp, "w") as out_file:
            for author in kfold.train_partitions[u]:
                author.export_comments2svm(trait, out_file, False)
            print("Finished exporting to {0}".format(svm_fp))

        # Export test datasets
        svm_name = feature_name + "_test_" + str(u)
        svm_fp = filepathes.svm_filepath(feature_name, svm_name)
        with open(svm_fp, "w") as out_file:
            for author in kfold.test_partitions[u]:
                author.export_comments2svm(trait, out_file, False)
            print("Finished exporting to {0}".format(svm_fp))


    # Use lexycal
    feature_name += "_lexycal"
    filepathes.build_svm_dir(feature_name)
    for u in range(k):
        # Export train datasets
        svm_name = feature_name + "_train_" + str(u)
        svm_fp = filepathes.svm_filepath(feature_name, svm_name)
        with open(svm_fp, "w") as out_file:
            for author in kfold.train_partitions[u]:
                author.export_comments2svm(trait, out_file, True)
            print("Finished exporting to {0}".format(svm_fp))

        # Export test datasets
        svm_name = feature_name + "_test_" + str(u)
        svm_fp = filepathes.svm_filepath(feature_name, svm_name)
        with open(svm_fp, "w") as out_file:
            for author in kfold.test_partitions[u]:
                author.export_comments2svm(trait, out_file, True)
            print("Finished exporting to {0}".format(svm_fp))


    print("Finished exporting all files.")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('feature_vector', metavar='m', type=str)
    parser.add_argument('trait', metavar='t', type=str)
    parser.add_argument('kfold', metavar='k', type=int)
    args = parser.parse_args()
    run(args.feature_vector, args.trait, args.kfold)

    # name examples
#    feature_name = "glove6B.300d"
#    feature_name = "Test_w2v_mean"
#    svm_name = "Test_w2v_mean_lexycal"
