import sys

def write(score_array, clf_name, file_p):
    simple_score = score_array[0:2]
    title = "General Accuracy"
    labels = [ clf_name]
    labels += ["Train", "Test"]
    table(labels, simple_score, title, file_p)
    
def table(labels, values, title, file_p):

    file_p.write("\\begin{center}\n")
    file_p.write("\\begin{tabular}\n")
    tuples = list(zip(labels, values))

    file_p.write("{|")
    for i in range(0, len(tuples)):
        file_p.write("c|")
    file_p.write("}\n")
        
    for tuple_ in tuples:
        file_p.write( "\\hline\n")
        for i, each in enumerate(tuple_):
            file_p.write(str(each))
            if (i == len(tuple_) - 1):
                file_p.write(" \\\\ ")
            else:
                file_p.write(" & ")
    file_p.write( "\\hline\n")
    file_p.write( "\\end{tabular}\n")
    file_p.write( "\\end{center}\n")

"""
def multirow_latex_tabular(score_array, algo_name):
    simple_score = score_array[0:3]
    ica_tuple_array = score_array[4]
    labels, ica_array = zip(*ica_tuple_array)
    ica_mean = np.mean(ica_array)
    ica_std = np.std(ica_array)

    simple_score.append(ica_mean)
    simple_score.append(ica_std)


    title = "General Accuracy"
    labels = ["Train", "Test", "Validation", "OC Mean", "OC STD"]
    classes = ["Bart", "Homer", "Lisa", "Maggie", "Marge"]

    file_p.write( "\\multirow{5}{*}{%s} & %s & %0.2f & %s & %0.2f\\\\\n"%(algo_name, labels[0], simple_score[0], classes[0], ica_array[0])

    for i in range(1, len(labels)):
        file_p.write( "& %s & %0.2f  & %s & %0.2f \\\\\n"%(labels[i], simple_score[i], classes[i], ica_array[i]))
    file_p.write( "\\hline\\hline\n")
"""
