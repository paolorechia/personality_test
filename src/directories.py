import sys
import os
import pickle

class FilepathesDB():
    def __init__(self):
        self.csv_dir = '../csv_data/'
        self.pickle_preprocessing_dir = '../preprocessed_data/'
        self.pickle_feature_vector_dir ='../features_data/'
        self.models_dir = '../models/'
        self.svm_file_dir = '../svm_files'
        self.charts_dir = '../charts'
        self.latex_dir= '../latex'
        self.results_dir= '../results'
        self.kfold_dir = '../kfold'
        self.csv_files_dict = {
            'essay' : 'essays.csv',
            'mypersonality': 'mypersonality_final.csv',
            # These two have already been preprocessed by lexycal analysis.
            'mypersonality_j': 'mypersonalityJunto07e.csv',
            'mypersonality_s':  'mypersonalitySeparado07e.csv',
        }

    def fetch_csv_path(self, csv_key):
        try:
            return os.path.join(self.csv_dir, self.csv_files_dict[csv_key])
        except:
            sys.exit("Invalid csv_key. Cannot fetch filepath.")

    # Pickle preprocessing filepath
    def preprocessing_filepath(self, name):
        name = name + ".pkl"
        return os.path.join(self.pickle_preprocessing_dir, name)

    def feature_filepath(self, name):
        name = name + ".pkl"
        return os.path.join(self.pickle_feature_vector_dir, name)

    def build_svm_dir(self, dir_n):
        path_dir = os.path.join(self.svm_file_dir, dir_n)
        if os.path.isdir(path_dir):
            return path_dir
        else:
            os.mkdir(os.path.join(self.svm_file_dir, dir_n))
            return path_dir

    def svm_dir(self, dir_n):
        return os.path.join(self.svm_file_dir, dir_n)

    def svm_filepath(self, dir_n, name):
        return os.path.join(self.svm_file_dir, dir_n, name)

    def w2v_model(self, name):
        return os.path.join(self.models_dir, name)

    def chart(self, name):
        name += ".pdf"
        return os.path.join(self.charts_dir, name)

    def latex(self, name):
        name += ".tex"
        return os.path.join(self.latex_dir, name)
    
    def result(self, name):
        name += ".pkl"
        return os.path.join(self.results_dir, name)

    def kfold(self, k):
        name = "kfold_" + str(k)
        return os.path.join(self.kfold_dir, name)

    def kfold_array(self, k):
        name = "mypersonalitySeparado.kf"
        name += str(k)
        return os.path.join(self.kfold_dir, name)
