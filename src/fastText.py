import os
import csv
import pprint
import sys
import pickle
import argparse
import gensim
import csv_classes
import logging
from subprocess import call

from directories import FilepathesDB

    # Program C

    # Read preprocessed text - done

    # Apply chosen K-Fold and split data

    # Applies different feature extraction techniques, storing them.

    # Reimplement iterator for training word2vec

# Only exists for mypersonality for now

def train_word2vec(comments_iterator):
    model = gensim.models.Word2Vec(comments_iterator, iter=1000)
    print(model.wv.most_similar(positive=['semester'], topn=6))
    w2v = dict(list(zip(model.wv.index2word, model.wv.syn0)))
    model_fp = filepathes.w2v_model("global_w2v")
    model.save(model_fp)

    model = gensim.models.Word2Vec.load(model_fp)
    print(model.wv.most_similar(positive=['semester'], topn=6))


    # Example of iterating over all the essay objects and feeding into Word2Vec
    # essay_iterator = EssayIterator(essay_list) # model = gensim.models.Word2Vec(essay_iterator)
    # print model.wv.most_similar(positive=['semester'], topn=6)

#    for co in comments_iterator:
#        for w in co:
#            sys.stdout.write(w)
#            sys.stdout.write(' ')
#        sys.stdout.write('\n')
#    sys.exit()
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

if __name__ == "__main__":
    filepathes = FilepathesDB()
    fp = filepathes.preprocessing_filepath("mypersonality")
    author_db = pickle.load(open(fp, 'rb'))
    print("{0} unique keys read from db.".format(len(list(author_db.authors.keys()))))
#   gensim fastText is so slow!!! ~20-30k words/sec VS 120k words/sec using facebook lib's implementation
#    model = gensim.models.FastText(comments_iterator)
    # Example of iterating over all the essay objects and feeding into Word2Vec
#    comments_iterator = csv_classes.CommentsIterator(author_db)
#    model = gensim.models.FastText(comments_iterator)
#    print model['semester']
#    train_word2vec(comments_iterator)
#    for co in comments_iterator:
#        for w in co:
#            sys.stdout.write(w)
#            sys.stdout.write(' ')
#        sys.stdout.write('\n')
#    sys.exit()
    fastText_path = "/home/paolo/develop/fastText/fastText/fasttext "
    command = "skipgram -input mypersonality.txt -output test/fil9 -lr 0.025 -dim 100 \
  -ws 5 -epoch 1000 -minCount 5 -neg 5 -loss ns -bucket 2000000 \
  -minn 3 -maxn 6 -thread 8 -t 1e-4 -lrUpdateRate 100"
    os.system(fastText_path + command)
