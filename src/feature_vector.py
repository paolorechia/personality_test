    # Program C - base code from http://nadbordrozd.github.io/blog/2016/05/20/text-classification-with-word2vec/

    # Read feature vector structure (if possible, keeping k-fold splitting)

    # Then feed into machine learning/Deep learning program

    # Generate metrics (report, graphs)

#get http://nlp.stanford.edu/data/glove.6B.zip
#unzip glove.6B.zip

#and use load them up in python:
import argparse
import numpy as np
import gensim
import pickle
import sys

import csv_classes
from directories import FilepathesDB
import pipeline_utils as pu

def str2bool(v):
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

from sklearn.pipeline import Pipeline
from sklearn.ensemble import ExtraTreesClassifier



def transform(word2vec, X, vec_size):
        sum_vec = np.zeros(vec_size)
        for w in X:
            if w in word2vec:
                sum_vec = sum_vec + word2vec[w]
            else:
                sum_vec = sum_vec + np.zeros(vec_size)
        try:
            result = np.divide(sum_vec, vec_size)
        except:
            return np.zeros(vez_size)
        return result


def run(model_name, output, pre_trained = False):
    filepathes = FilepathesDB()
    model_fp = filepathes.w2v_model(model_name)

    if not pre_trained:
        model = gensim.models.Word2Vec.load(model_fp)
        w2v = dict(list(zip(model.wv.index2word, model.wv.syn0)))
    else:
        with open(model_fp, "rb") as lines:
            w2v = {line.split()[0]: np.array(list(map(float, line.split()[1:])))
                   for line in lines}
        
    fp = filepathes.preprocessing_filepath("mypersonality")
    author_db = pickle.load(open(fp, 'rb'))
    print("{0} unique keys read from db.".format(len(list(author_db.authors.keys()))))

    comments_iterator = csv_classes.RootCommentsIterator(author_db)
    i = 0
    dimension = 300
    for comment in comments_iterator:
        comment.set_feature_vector(transform(w2v, comment.preprocessed_text, dimension))
        i += 1
    print("{0} feature vectors generated for DB. Congrats!".format(i))
    author_db.pickle_save(filepathes.feature_filepath(output))

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('input_model', metavar='m', type=str)
    parser.add_argument('pre_trained', type=str2bool, nargs='?',
                         const=True, default=False,
                         metavar='t')
    args = parser.parse_args()
    output = args.input_model
#    model_name = "glove.6B.300d.txt"
#    output = "glove6B.300d"
    run(args.input_model, output, args.pre_trained)
